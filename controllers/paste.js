const Content = require('../models/content');

exports.getContent = (req, res, next) => {
  const contId = req.params.id;
  Content.findById(contId)
    .then(content => {
      res.json(content);
    })
    .catch(err => {
      res.status(404).send();
    });
};

exports.postContent = (req, res, next) => {
  const title = req.body.title;
  const content = req.body.content;
  const newContent = new Content({
    title: title,
    content: content
  });
  newContent
    .save()
    .then(result => {
      console.log('Created Content');
      res.send({
        id: newContent._id
      });
    })
    .catch(err => {
      res.status(400).send({
        error: err
      });
    });
};

exports.postRecents = (req, res, next) => {
  Content.find({})
    .sort({ createdAt: 'desc' })
    .limit(100)
    .exec((err, docs) => {
      res.json(docs);
    });
};
