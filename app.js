const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const routes = require('./routes/index');

// const config = require('./config/Config');

const app = express();

app.use(bodyParser.json());

app.use('/', routes);

mongoose
  .connect('mongodb://mongo:27017/pastebin', {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(result => {
    app.listen(3000);
  })
  .catch(err => {
    console.log(err);
  });

module.exports = app;
