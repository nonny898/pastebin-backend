const express = require('express');
const router = express.Router();
const pasteController = require('../controllers/paste');

router.get('/api/:id', pasteController.getContent);

router.post('/api/paste', pasteController.postContent);

router.post('/api/recents', pasteController.postRecents);

module.exports = router;
