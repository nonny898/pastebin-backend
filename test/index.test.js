const app = require('../app');
const supertest = require('supertest');
const mongoose = require('mongoose');
const request = supertest(app);

afterEach(async () => {
  await mongoose.connection.collections.contents.drop();
});

describe('POST /api/paste', () => {
  it('create new content', async done => {
    const response = await request.post('/api/paste').send({
      title: 'title',
      content: 'content'
    });
    expect(response.status).toBe(200);
    expect(response.body.id).toBeDefined();
    done();
  });
});
describe('POST /api/recent', () => {
  it('post recents', async done => {
    await request.post('/api/paste').send({
      title: 'title',
      content: 'content'
    });
    const response = await request.post('/api/recents');
    expect(response.body.length).toBe(1);
    done();
  });
});
describe('GET /api/:id', () => {
  it('get id', async done => {
    const apiPaste = await request.post('/api/paste').send({
      title: 'title',
      content: 'content'
    });
    const response = await request.get(`/api/${apiPaste.body.id}`);
    expect(response.body.title).toBe('title');
    expect(response.body.content).toBe('content');
    expect(response.body.createdAt).toBeDefined();
    done();
  });
});
describe.skip('post /api/paste', () => {
  it('get id', async done => {
    const array = [];
    for (let respose = 0; respose < 500; respose++) {
      array.push(
        await request.post('/api/paste').send({
          title: 'title',
          content: 'content'
        })
      );
    }
    const responses = await Promise.all(array);
    console.log(responses);
    expect(responses.length).toBe(500);
    done();
  });
});
